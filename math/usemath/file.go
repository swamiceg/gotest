package usemath

import "fmt"
import "bitbucket.org/swamiceg/gotest/math"

func Run() {
    s := math.Sum(10, 10)
    fmt.Println("Added extra line")
    fmt.Println("Calculated sum using math:", s)
}